class Student

attr_accessor :first_name , :last_name , :courses

def initialize (first_name , last_name)
  @first_name = first_name
  @last_name = last_name
  @courses = []
end

def name
  @first_name + " " + @last_name
end

def enroll(new_course)
  if !@courses.include?(new_course)
      @courses.each do |course|
        if   course.conflicts_with?(new_course)
          raise "Already enrolled course"
        end
      end
      @courses.push(new_course)
      new_course.students.push(self)
  end
end

def courses
  @courses
end

def course_load
  hash = Hash.new(0)

  courses.each do |course|
    hash[course.department] += course.credits
  end

  hash
end


end
